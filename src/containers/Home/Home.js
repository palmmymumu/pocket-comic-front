import React, { Component } from 'react'
import { Container, Row, Col, Carousel, CarouselItem, CarouselControl, CarouselIndicators, CarouselCaption } from 'reactstrap'
import { appConfig } from '../../config'

import BookItem from '../BookItem/BookItem'
import Footer from '../../components/Footer/Footer'
import SearchBar from '../../components/SearchBar/SearchBar'

class Home extends Component {
	state = { activeIndex: 0, searchText: '' }

	onExiting = () => {
		this.animating = true
	}

	onExited = () => {
		this.animating = false
	}

	next = () => {
		if (this.animating) return
		const { carouselItems } = appConfig
		const nextIndex = this.state.activeIndex === carouselItems.length - 1 ? 0 : this.state.activeIndex + 1
		this.setState({ activeIndex: nextIndex })
	}

	previous = () => {
		if (this.animating) return
		const { carouselItems } = appConfig
		const nextIndex = this.state.activeIndex === 0 ? carouselItems.length - 1 : this.state.activeIndex - 1
		this.setState({ activeIndex: nextIndex })
	}

	goToIndex = newIndex => {
		if (this.animating) return
		this.setState({ activeIndex: newIndex })
	}

	handleChange = e => {
		if (e) {
			const name = e.target.id
			const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value
			this.setState({
				[name]: value
			})
		}
	}

	render = () => {
		const { activeIndex, searchOpen, searchText } = this.state
		const { carouselItems, bookItems } = appConfig

		const sortedBook = bookItems.sort((a, b) => +b.id - +a.id).slice(0, 9)
		const filteredBook = bookItems.filter(book => book.name.indexOf(searchText) !== -1).sort((a, b) => +b.id - +a.id)

		return (
			<div>
				<SearchBar searchText={searchText} handleChange={this.handleChange} />
				{searchText.trim() === '' && (
					<Carousel activeIndex={activeIndex} next={this.next} previous={this.previous}>
						<CarouselIndicators items={carouselItems} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
						{carouselItems.map(item => {
							return (
								<CarouselItem onExiting={this.onExiting} onExited={this.onExited} key={item.src}>
									<div style={{ height: '270px', backgroundImage: `url(${item.src})`, backgroundSize: 'cover' }} />
									<CarouselCaption captionText={item.caption} captionHeader={item.caption} />
								</CarouselItem>
							)
						})}
						<CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
						<CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
					</Carousel>
				)}
				<Container className="mt-2">
					{searchText.trim() === '' && (
						<Row className="pb-2 mb-2 hr">
							<Col>
								<h5 className="m-0 mt-2 text-danger">หนังสือเข้าใหม่</h5>
							</Col>
						</Row>
					)}
					{searchText.trim() === '' && (
						<Row className="px-1">
							{sortedBook.map(book => (
								<BookItem book={book} />
							))}
						</Row>
					)}
					{searchText.trim() !== '' && (
						<center>
							<div className="search-result mb-2">
								<small>ผลการค้นหาทั้งหมด {filteredBook.length} รายการ</small>
							</div>
						</center>
					)}
					{searchText.trim() !== '' && (
						<Row className="px-1">
							{filteredBook.map(book => (
								<BookItem book={book} />
							))}
						</Row>
					)}
					<Footer />
				</Container>
			</div>
		)
	}
}

export default Home
