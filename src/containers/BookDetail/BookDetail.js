import React, { Component } from 'react'
import { Redirect, withRouter } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap'
import Lightbox from 'react-image-lightbox'

import { appConfig } from '../../config'

import Footer from '../../components/Footer/Footer'
import BookItem from '../BookItem/BookItem'

class BookDetail extends Component {
	state = { isOpen: false, imageIndex: 0 }

	componentWillMount = () => {
		this.scrollTop()
	}

	componentWillReceiveProps = () => {
		this.scrollTop()
	}

	scrollTop = () => {
		window.scrollTo({
			top: 0
		})
	}

	render() {
		const { isOpen, imageIndex } = this.state
		const {
			match: {
				params: { id }
			}
		} = this.props
		const { bookItems } = appConfig
		const book = bookItems.filter(book => +id === +book.id).pop()

		if (!book) {
			return <Redirect to="/" />
		}

		const keyword = book.name
			.split(' ')
			.sort((a, b) => a.length - b.length)
			.pop()

		const suggestedBook = bookItems
			.filter(book => +book.id !== +id && book.name.indexOf(keyword) !== -1)
			.sort((a, b) => +b.id - +a.id)
			.slice(-6)

		return (
			<Container className="mt-3">
				<Row className="pb-3 mb-3 hr">
					<Col className="pr-0" md={2} sm={4} xs={5}>
						<img className="w-100" src={book.thumbnail_image} alt={book.name} />
					</Col>
					<Col md={10} sm={8} xs={7}>
						<h5 className="m-0 text-danger">{book.name}</h5>
						<div className="p">
							<small>{book.desc}</small>
						</div>
					</Col>
				</Row>
				<Row>
					<Col>
						{book.images.map((image, index) => (
							<div className="image-desc mb-3" onClick={() => this.setState({ isOpen: true, imageIndex: index })}>
								<center>
									<img className="w-100" src={image} alt={book.name} />
								</center>
								<div className="desc top px-2">
									<small>
										{index + 1} of {book.images.length}
									</small>
								</div>
							</div>
						))}
					</Col>
				</Row>
				{suggestedBook.length !== 0 && (
					<Row className="pb-2 mb-2 hr">
						<Col>
							<h5 className="m-0 mt-2 text-danger">หนังสือที่คล้ายกัน</h5>
						</Col>
					</Row>
				)}
				{suggestedBook.length !== 0 && (
					<Row className="px-1">
						{suggestedBook.map(book => (
							<BookItem book={book} />
						))}
					</Row>
				)}
				<Footer />
				{isOpen && (
					<Lightbox
						mainSrc={book.images[imageIndex]}
						nextSrc={book.images[(imageIndex + 1) % book.images.length]}
						prevSrc={book.images[(imageIndex + book.images.length - 1) % book.images.length]}
						onCloseRequest={() => this.setState({ isOpen: false })}
						onMovePrevRequest={() =>
							this.setState({
								imageIndex: (imageIndex + book.images.length - 1) % book.images.length
							})
						}
						onMoveNextRequest={() =>
							this.setState({
								imageIndex: (imageIndex + 1) % book.images.length
							})
						}
					/>
				)}
			</Container>
		)
	}
}

export default withRouter(BookDetail)
