import React, { Component } from 'react'
import { Redirect, withRouter } from 'react-router-dom'

class SignOut extends Component {
	componentDidMount = () => {
		window.firebase.signOut()
	}

	render() {
		return <Redirect to="/signin" />
	}
}

export default withRouter(SignOut)
