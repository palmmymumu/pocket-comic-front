import React, { Component } from 'react'
import { Container, Row, Col, FormFeedback, Input } from 'reactstrap'
import { Route, Redirect, withRouter, Link } from 'react-router-dom'
import BlockUi from 'react-block-ui'

class SignUp extends Component {
	state = {
		email: '',
		password: '',
		password2: '',
		tos: false,
		isLoading: false
	}

	handleChange = e => {
		if (e) {
			const name = e.target.id
			const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value
			this.setState({
				[name]: value
			})
		}
	}

	handleSignUp = e => {
		if (e) {
			e.preventDefault()
			const { email, password, password2 } = this.state
			const { history } = this.props

			if (password !== password2) {
				return window.alert('Password and Repeat Password is not match.')
			}

			this.setState({ isLoading: true }, () =>
				window.firebase
					.createUserWithEmailAndPassword(email, password)
					.then(() => history.push('/'))
					.catch(err => window.alert(err.message))
					.finally(() => this.setState({ isLoading: false }))
			)
		}
	}

	render() {
		const { email, password, password2, tos, isLoading } = this.state

		return (
			<Container>
				<Row>
					<Col md={{ size: 4, offset: 4 }} className="mt-4">
						<h3 className="text-center text-danger">สมัครสมาชิก</h3>
						<BlockUi blocking={isLoading}>
							<form onSubmit={this.handleSignUp} className="mb-3">
								<Row>
									<Col>
										<div className="form-group">
											<label for="email">อีเมล</label>
											<Input
												type="email"
												className="form-control"
												id="email"
												onChange={this.handleChange}
												value={email}
												placeholder="Email Address"
												required
											/>
										</div>
									</Col>
								</Row>
								<Row>
									<Col>
										<div className="form-group">
											<label for="password">รหัสผ่าน</label>
											<Input
												type="password"
												className="form-control"
												id="password"
												onChange={this.handleChange}
												value={password}
												placeholder="Password"
												required
											/>
										</div>
									</Col>
								</Row>
								<Row>
									<Col>
										<div className="form-group">
											<label for="password">รหัสผ่านอีกครั้ง</label>
											<Input
												type="password"
												className="form-control"
												id="password2"
												onChange={this.handleChange}
												value={password2}
												placeholder="Repeat Password"
												required
											/>
										</div>
									</Col>
								</Row>
								{/* <Row>
									<Col>
										<div className="form-check">
											<div className="checkbox">
												<label>
													<Input type="checkbox" id="tos" onChange={this.handleChange} checked={tos} /> ยอมรับ
													<a href="">ข้อตกลงการให้บริการ</a>
												</label>
											</div>
										</div>
									</Col>
								</Row> */}
								<button type="submit" className="btn btn-block btn-danger">
									<i className="fa fa-user-plus" /> สมัครสมาชิก
								</button>
							</form>
						</BlockUi>
						เป็นสมาชิกอยู่แล้ว ? <Link to="/signup">เข้าสู่ระบบ</Link>
					</Col>
				</Row>
			</Container>
		)
	}
}

export default withRouter(SignUp)
