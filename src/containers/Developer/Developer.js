import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap'

import { appConfig } from '../../config'
import Footer from '../../components/Footer/Footer'

class Developer extends Component {
	render() {
		const { developers } = appConfig
		return (
			<Container className="mt-2">
				<Row>
					{developers
						.sort((a, b) => +a.id - +b.id)
						.map(dev => (
							<Col md={4} sm={6} xs={12} className="my-2">
								<div className="image-desc">
									<a target="_blank" href={dev.url}>
										<img src={dev.src} className="w-100" />
										<div className="desc py-1 px-2">
											{dev.name}
											<br />
											<small>
												{dev.id} ({dev.position})
											</small>
										</div>
									</a>
								</div>
							</Col>
						))}
				</Row>
				<Footer />
			</Container>
		)
	}
}
export default Developer
