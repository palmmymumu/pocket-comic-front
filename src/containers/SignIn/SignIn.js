import React, { Component } from 'react'
import { Container, Row, Col, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import { Route, Redirect, withRouter, Link } from 'react-router-dom'
import BlockUi from 'react-block-ui'

class SignIn extends Component {
	state = {
		email: '',
		password: '',
		isLoading: false
	}

	handleChange = e => {
		if (e) {
			this.setState({
				[e.target.id]: e.target.value
			})
		}
	}

	handleLogin = e => {
		if (e) e.preventDefault()
		const { email, password } = this.state
		const { history } = this.props

		this.setState({ isLoading: true }, () =>
			window.firebase
				.signInWithEmailAndPassword(email, password)
				.then(response => history.push('/'))
				.catch(error => window.alert(error.message))
				.finally(() => this.setState({ isLoading: false }))
		)
	}

	render() {
		const { email, password, isLoading } = this.state

		return (
			<Container>
				<Row>
					<Col md={{ size: 4, offset: 4 }} className="mt-4">
						<h3 className="text-center text-danger">เข้าสู่ระบบ</h3>
						<BlockUi blocking={isLoading}>
							<form onSubmit={this.handleLogin} className="mb-3">
								<div className="form-group">
									<label for="email">อีเมล</label>
									<input type="email" className="form-control" id="email" onChange={this.handleChange} value={email} />
								</div>
								<div className="form-group">
									<label for="password">รหัสผ่าน</label>
									<input type="password" className="form-control" id="password" onChange={this.handleChange} value={password} />
								</div>
								<button type="submit" className="btn btn-block btn-danger">
									<i className="fa fa-sign-in" /> เข้าสู่ระบบ
								</button>
							</form>
						</BlockUi>
						ยังไม่ได้เป็นสมาชิก? <Link to="/signup">สมัครสมาชิก</Link>
					</Col>
				</Row>
			</Container>
		)
	}
}

export default withRouter(SignIn)
