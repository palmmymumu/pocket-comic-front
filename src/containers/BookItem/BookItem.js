import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { Col } from 'reactstrap'

class BookItem extends Component {
	render() {
		const { book, history } = this.props

		return (
			<Col md={2} sm={4} xs={4} className="my-1 px-1" onClick={() => history.push(`/book/${book.id}`)}>
				<div className="image-desc">
					<img className="w-100" src={book.thumbnail_image} />
					<div className="desc text-center">
						<small>{book.name}</small>
					</div>
				</div>
			</Col>
		)
	}
}

export default withRouter(BookItem)
