import React, { Component } from 'react'
import { Switch, Router, Route } from 'react-router-dom'

import firebase from 'firebase'
import createHistory from 'history/createBrowserHistory'

import { appConfig } from './config'

import NavBar from './components/NavBar/NavBar'
import Home from './containers/Home/Home'
import SignIn from './containers/SignIn/SignIn'
import SignUp from './containers/SignUp/SignUp'
import SignOut from './containers/SignOut/SignOut'
import NotFound from './containers/NotFound/NotFound'
import BookDetail from './containers/BookDetail/BookDetail'
import Developer from './containers/Developer/Developer'
import Footer from './components/Footer/Footer'

class App extends Component {
	constructor(props) {
		super(props)
		firebase.initializeApp(appConfig.firebase)
		window.firebase = firebase.auth()
	}

	render() {
		return (
			<Router history={createHistory()}>
				<div>
					<NavBar />
					<Switch>
						<Route exact path="/" component={Home} />
						<Route exact path="/signin" component={SignIn} />
						<Route exact path="/signup" component={SignUp} />
						<Route exact path="/signout" component={SignOut} />
						<Route exact path="/book/:id" component={BookDetail} />
						<Route exact path="/dev" component={Developer} />
						<Route component={NotFound} />
					</Switch>
				</div>
			</Router>
		)
	}
}

export default App
