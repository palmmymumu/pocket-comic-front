export const appConfig = {
	firebase: {
		apiKey: 'AIzaSyDlxYbm-IxT4xBMLLe8ZwcPdYdYxtXl9bk',
		authDomain: 'webapp-850dc.firebaseapp.com',
		databaseURL: 'https://webapp-850dc.firebaseio.com',
		projectId: 'webapp-850dc',
		storageBucket: 'webapp-850dc.appspot.com',
		messagingSenderId: '581602888888'
	},
	carouselItems: [
		{ src: 'http://www.freaksugar.com/wp-content/uploads/2014/07/batman-66a-comic-header.jpg' },
		{ src: 'https://didyouseethatone.files.wordpress.com/2013/06/comic-header.jpg' },
		{ src: 'http://headersfortwitter.com/wp-content/uploads/2012/10/vintage-comic-strip-smoking.jpg' }
	],
	bookItems: [
		{
			id: 1,
			name: 'Deadpool #1',
			desc:
				'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum',
			thumbnail_image: 'https://i.pinimg.com/originals/2f/06/fd/2f06fd03cb6cd301ef897065bc0d18dd.jpg',
			images: [
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg'
			]
		},
		{
			id: 2,
			name: 'Deadpool #2',
			desc:
				'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum',
			thumbnail_image: 'https://i.pinimg.com/originals/73/09/85/730985e59618be96b8c5a1903a42d950.jpg',
			images: [
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg'
			]
		},
		{
			id: 3,
			name: 'Deadpool #3',
			desc:
				'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum',
			thumbnail_image: 'https://i.pinimg.com/originals/8e/a8/95/8ea8956bc6a536874f471a99e311e6e5.jpg',
			images: [
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg'
			]
		},
		{
			id: 4,
			name: 'Superman #1',
			desc:
				'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum',
			thumbnail_image: 'http://img.chelmerfineart.com/extra-large/the-invincible-iron-man-81-the-deep-end-part-3-21204.jpg',
			images: [
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg'
			]
		},
		{
			id: 5,
			name: 'Superman #2',
			desc:
				'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum',
			thumbnail_image: 'https://senseofrightalliance.files.wordpress.com/2011/02/avengers-prime.jpg',
			images: [
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg'
			]
		},
		{
			id: 6,
			name: 'Superman #3',
			desc:
				'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum',
			thumbnail_image:
				'https://imgcs.artprintimages.com/img/print/print/marvel-comics-retro-captain-america-comic-book-cover-no-112-album-issue-aged_a-l-14823077-11969363.jpg?w=550&h=550',
			images: [
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg',
				'https://static.posters.cz/image/1300/wall-murals/avengers-comic-marvel-i21603.jpg'
			]
		}
	],
	developers: [
		{
			id: '5835512030',
			name: 'นายพงศธร วีระตุมมา',
			src:
				'https://scontent.fbkk14-1.fna.fbcdn.net/v/t31.0-8/21743779_2003573986586741_8390271665754873016_o.jpg?_nc_cat=100&_nc_eui2=AeE0qxXcm11HcsROWUT2WOWmH2Nxlv3uTkSPJsz4XgXvlTy90On8M_K2aduIP2iS4PGKn82GPW4-BlwFbQOhhQKFCzDQk9NtyA6BPAkMLR1d9w&_nc_ht=scontent.fbkk14-1.fna&oh=22d5a13ec337dd07f83c4942860f46e0&oe=5CAD3B09',
			position: 'Developer',
			url: 'https://www.facebook.com/palmz.haxker'
		},
		{
			id: '5835512012',
			name: 'นายธีรวุธ เสียมไหม',
			src:
				'https://scontent.fbkk10-1.fna.fbcdn.net/v/t31.0-8/14633105_1768377880117443_3266286466432213357_o.jpg?_nc_cat=101&_nc_eui2=AeHzxGm9XZc6FE7Z21ysM-uS61FTwKMD7eTIThujzRZXPdLCM3C_VYUuGQu4-Qyo8QMKFU_gn6ECvDktiDQpF6RlZHcRZNXbjyBKV8timmVEGA&_nc_ht=scontent.fbkk10-1.fna&oh=99546569f8448139c3b306b629d06765&oe=5C75346A',
			position: 'Developer',
			url: 'https://www.facebook.com/palmz.haxker'
		},
		{
			id: '5835512023',
			name: 'นายกฤตเมธ ลิ่วทิพา',
			src:
				'https://scontent.fbkk10-1.fna.fbcdn.net/v/t31.0-8/22712227_1550368305022654_400991693328796433_o.jpg?_nc_cat=102&_nc_eui2=AeFzI6hpPmmSr0TixC9hsfj1cD7LIJIlTW3kN3hUCubGE-oZ8FXSjIYKXQMJkaQ57VwAnpLhqR-gY5UNYb3Zf9XKio8SWofmvSkS-j91rwIJNg&_nc_ht=scontent.fbkk10-1.fna&oh=a16b864be47099693bf7f32713f9a41d&oe=5C666415',
			position: 'Developer',
			url: 'https://www.facebook.com/palmz.haxker'
		},
		{
			id: '5835512124',
			name: 'นางสาวกานต์กนก มณีโชติ',
			src:
				'https://scontent.fbkk14-1.fna.fbcdn.net/v/t1.0-9/18699788_1446692152054492_8005395154686431620_n.jpg?_nc_cat=111&_nc_eui2=AeGOm7uvOkSwl26uBptz7ZGbAU3wMgrDFq4Q-sRX3xlloqnbbAd6xksv6g8MKX99qCz9z7yOe_7_ifLNH9qpEKBQovP4OdQmZkCjem44AbpyLg&_nc_ht=scontent.fbkk14-1.fna&oh=b3508c385ffff2d68ee762df1130a09c&oe=5CAC4AA0',
			position: 'Developer',
			url: 'https://www.facebook.com/palmz.haxker'
		},
		{
			id: '5835512025',
			name: 'นายนราวิชญ์ ทองเพ็ชร',
			src:
				'https://scontent.fbkk12-2.fna.fbcdn.net/v/t1.0-9/30581900_1803055533050275_8355793586570133504_o.jpg?_nc_cat=105&_nc_eui2=AeGTHc44LRtVe9cEMVm6Na2R4JRQhmTLFOOwbbtj4zxB2-BOLpXzO-jHkXnTpjtEch4p2X-swDXGnvVwEIYJNZA3D_7D0-OUe2qeOgtz0Kox-A&_nc_ht=scontent.fbkk12-2.fna&oh=072b817b2959d71176a2277fc69738e8&oe=5C7AC45B',
			position: 'Developer',
			url: 'https://www.facebook.com/skipervitzaa'
		},
		{
			id: '5835512020',
			name: 'นายกิตตินัน ชาญปรีชา',
			src:
				'https://scontent.fbkk14-1.fna.fbcdn.net/v/t31.0-8/30052266_1627571430644825_5782566984902516984_o.jpg?_nc_cat=109&_nc_eui2=AeFeZqOSNF0vW4e1j4I8k8Z6az2AACXZcZFjBHtvR5XH9roLIc9PBg2hl68fty5g528q1av7fwqAcX8Ftiz5_6rXMSrLDP8XWJpY_Zw-llkMJw&_nc_ht=scontent.fbkk14-1.fna&oh=4082f0448bcf95a2ce669be99da11556&oe=5CAD4F2C',
			position: 'Developer',
			url: 'https://www.facebook.com/ggbandit'
		}
	]
}
