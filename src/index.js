import React from 'react'
import { render } from 'react-dom'
import App from './App'

import 'bootstrap/dist/css/bootstrap.css'
import 'font-awesome/css/font-awesome.min.css'
import 'react-block-ui/style.css'
import './styles/global.css'
import 'react-image-lightbox/style.css'

import registerServiceWorker from './registerServiceWorker'

const startApp = () => {
	render(<App />, document.getElementById('root'))
	registerServiceWorker()
}

if (window.cordova) {
	document.addEventListener('deviceready', startApp, false)
} else {
	startApp()
}
