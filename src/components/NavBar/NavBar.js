import React, { Component } from 'react'
import {
	Container,
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
} from 'reactstrap'
import { Link } from 'react-router-dom'
import enhanceWithClickOutside from 'react-click-outside'

import navigationModel from '../../config/navigation'

class NavBar extends Component {
	state = {
		isOpen: false,
		isUserMenuOpen: false,
		user: null
	}

	handleClickOutside = () => {
		this.setState({ isOpen: false })
	}

	componentDidMount() {
		window.firebase.onAuthStateChanged(user => this.setState({ user }))
	}

	componentWillReceiveProps = () => {
		this.setState({ isOpen: false })
	}

	toggle = () => {
		const { isOpen } = this.state
		this.setState({ isOpen: !isOpen })
	}

	forceClose = () => {
		this.setState({ isOpen: false, isUserMenuOpen: false })
	}

	toggleUserMenu = () => {
		const { isUserMenuOpen } = this.state
		this.setState({ isUserMenuOpen: !isUserMenuOpen })
	}

	render() {
		const { isOpen, isUserMenuOpen, user } = this.state
		const { brand, menus } = navigationModel

		console.log('User', user)

		return (
			<Navbar className="nav-top text-white" light expand="md">
				<Container>
					<Link to="/">
						<div className="navbar-brand font-weight-bold">{brand}</div>
					</Link>
					<NavbarToggler onClick={() => this.toggle()} />
					<Collapse isOpen={isOpen} navbar>
						<Nav navbar>
							{menus.map(menu => {
								if (!menu.role || menu.role === (user && user.role)) {
									return (
										<NavItem key={menu.label}>
											<Link onClick={() => this.forceClose()} className="nav-link" to={menu.link}>
												<i className={menu.icon} /> {menu.label}
											</Link>
										</NavItem>
									)
								}
								return null
							})}
						</Nav>
						<Nav navbar className="ml-auto">
							{!user && (
								<NavItem>
									<Link onClick={() => this.forceClose()} className="nav-link" to="/signin">
										<i className="fa fa-sign-in" /> เข้าสู่ระบบ
									</Link>
								</NavItem>
							)}
							{!user && (
								<NavItem>
									<Link onClick={() => this.forceClose()} className="nav-link" to="/signup">
										<i className="fa fa-user-plus" /> สมัครสมาชิก
									</Link>
								</NavItem>
							)}
							{user && (
								<UncontrolledDropdown isOpen={isUserMenuOpen}>
									<DropdownToggle onClick={() => this.toggleUserMenu()} tag="a" className="nav-link" caret>
										<i className="fa fa-user" /> {user.email}
									</DropdownToggle>
									<DropdownMenu>
										<Link onClick={() => this.forceClose()} className="dropdown-item" to="/signout">
											<i className="fa fa-sign-out" /> ออกจากระบบ
										</Link>
									</DropdownMenu>
								</UncontrolledDropdown>
							)}
							{/* {user && (
								<NavItem active={'Me' === currentView}>
									<Link onClick={() => this.forceClose()} className="nav-link" to={`/user/${user.user_id}/${encodeURIFriendly(user.username)}`}>
										<i className="fa fa-user" /> {user.username}
									</Link>
								</NavItem>
							)} */}
							{/* {user && (
								<NavItem active={'SignOut' === currentView}>
									<Link onClick={() => this.forceClose()} className="nav-link" to="/signout">
										<i className="fa fa-sign-out" /> ออกจากระบบ
									</Link>
								</NavItem>
							)} */}
						</Nav>
					</Collapse>
				</Container>
			</Navbar>
		)
	}
}

export default enhanceWithClickOutside(NavBar)
