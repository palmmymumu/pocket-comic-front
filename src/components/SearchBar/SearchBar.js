import React, { Component } from 'react'
import { Container } from 'reactstrap'
import enhanceWithClickOutside from 'react-click-outside'

class SearchBar extends Component {
	state = { isOpen: false }

	handleClickOutside = () => {
		this.setState({ isOpen: false })
	}

	toggle = () => {
		const { isOpen } = this.state
		this.setState({ isOpen: !isOpen })
	}

	render() {
		const { isOpen } = this.state
		const { searchText, handleChange } = this.props
		return (
			<div className="search-bar py-2">
				<Container>
					<a className={`search-btn ${isOpen ? 'd-none' : ''}`} onClick={this.toggle}>
						<i className="fa fa-search" /> Search book...
					</a>
					<input
						type="text"
						className={`form-control ${isOpen ? '' : 'd-none'}`}
						id="searchText"
						onChange={handleChange}
						placeholder="Search"
						value={searchText}
					/>
				</Container>
			</div>
		)
	}
}

export default enhanceWithClickOutside(SearchBar)
