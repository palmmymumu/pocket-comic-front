import React, { Component } from 'react'
import { Row, Col } from 'reactstrap'
import { Link } from 'react-router-dom'

export default class Footer extends Component {
	render() {
		return (
			<Row className="mt-2">
				<Col>
					<p className="text-center">
						<small>
							Copyright &copy; 2018-2022, All rights reserved.
							<br />
							<Link to="/dev">Developer Team</Link>
						</small>
					</p>
				</Col>
			</Row>
		)
	}
}
